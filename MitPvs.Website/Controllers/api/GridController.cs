﻿using MitPvs.Feature.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Services;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    //[Route("api/widget/[controller]")]
    [Route("api/[controller]")]
    public class GridController : UmbracoApiController
    {
        private IAccessTokenManager accessTokenService = new AccessTokenManager();

        // GET: GetWidgetGrid
        [HttpGet]
        public string GetGrid(string token)
        {
            var ms = Services.MemberService;
            var member = accessTokenService.GetMemeberByToken(token);
            // grid data in json
            var grid = member.GetValue<string>("grid");

            return grid;
        }

        public class Position
        {
            public string Token { get; set; }
            public string Grid { get; set; }
            public bool Success { get; set; }
        }
        [HttpPost]
        //update the grid from VUE to Umbraco on the user
        public object UpdateGrid(Position data)
        {
            var ms = Services.MemberService;
            var member = accessTokenService.GetMemeberByToken(data.Token);
            
            // save the new grid to Umbraco on the user
            member.SetValue("grid", data.Grid);
            ms.Save(member);
            data.Success = true;
            
            // return the changes that are made
            return data;
        }
    }
}