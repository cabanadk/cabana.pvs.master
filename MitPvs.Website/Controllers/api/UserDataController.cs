﻿using MitPvs.Feature.Managers;
using MitPvs.Website.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    [Route("api/[controller]")]
    public class UserDataController : UmbracoApiController
    {
        private IAccessTokenManager accessTokenService = new AccessTokenManager();

        [HttpGet]
        public object GetAllData(string token)
        {
            var currentMember = accessTokenService.GetMemeberByToken(token);
            if (currentMember == null)
            {
                return false;
            }
            var jObject = new JObject();
            var jArray = new JArray();
            var jArray1 = new JArray();

            foreach (var item in currentMember.Properties)
            {

                switch (item.Alias)
                {
                    case ("pvsMemberFirstName"):
                        if (item.Value != null)
                        {
                            jObject.Add("firstName", item.Value.ToString());
                        }
                        break;
                    case ("pvsMemberLastName"):
                        if (item.Value != null)
                        {
                            jObject.Add("lastName", item.Value.ToString());
                        }
                        break;
                    case ("pvsMemberTelephoneNumber"):
                        if (item.Value != null)
                        {
                            jObject.Add("number", item.Value.ToString());
                        }
                        break;
                    case ("pvsAlternativeEmail"):
                        if (item.Value != null)
                        {
                            jObject.Add("altEmail", item.Value.ToString());
                        }
                        break;
                    case ("pvsAccessToken"):
                        if (item.Value != null)
                        {
                            jObject.Add("token", item.Value.ToString());
                        }
                        break;
                    case ("grid"):
                        if (item.Value != null)
                        {
                            jObject.Add("grid", item.Value.ToString());
                        }
                        break;
                    case ("pvsMemberRightArea"):
                        foreach (var value in item.Value.ToString().Split(','))
                        {
                            var text = umbraco.library.GetPreValueAsString(int.Parse(value.ToString()));

                            jArray.Add(text);

                        }
                        break;
                    case ("pvsMemberCategory"):

                        var text1 = umbraco.library.GetPreValueAsString(int.Parse(item.Value.ToString())) != null ? umbraco.library.GetPreValueAsString(int.Parse(item.Value.ToString())) : " ";

                        jObject.Add("category", text1);
                        break;
                }

            }
            jObject.Add("Right Area", jArray);
            jObject.Add("email", currentMember.Email);
            Status status = new Status();

            try
            {
                status.Success = true;
                status.Msg = "User " + currentMember.Name;
                return jObject;
            }
            catch (Exception ex)
            {
                status.Success = false;
                status.Msg = "Failed to get User " + ex;
                return status;
                throw new Exception("Failed to get member data ", ex);
            }
        }

        [HttpGet]
        public Status DeleteAllData(string token)
        {
            Status status = new Status();
            var currentMember = accessTokenService.GetMemeberByToken(token);
            var ms = Services.MemberService;
            ms.Delete(currentMember);

            status.Msg = "User " + currentMember.Name + " is deleted";
            status.Success = true;
            return status;
        }
    }
}