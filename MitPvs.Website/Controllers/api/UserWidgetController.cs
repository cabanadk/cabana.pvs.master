﻿using MitPvs.Feature.Managers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using Umbraco.Core.Persistence;
using Umbraco.Core.Services;
using Umbraco.Web.PublishedCache;
using Umbraco.Web.Security;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    [Route("api/[controller]")]
    public class UserWidgetController:UmbracoApiController
    {
        public class Status
        {
            public bool Success { get; set; }
            public string Msg { get; set; }
        }
        private IAccessTokenManager accessTokenService = new AccessTokenManager();
        public class Data
        {
            public int Id { get; set; }
            public string Token { get; set; }
            public string Title { get; set; }
            public string Link { get; set; }
            public string Selected { get; set; }
            public string Open { get; set; }
            public int TabNumber { get; set; }
        }


        [HttpPost]
        public object WidgetHistoryLinks(Data model)
        {
            Status status = new Status();
            var ms = ApplicationContext.Services.MemberService;
           
            var currentMember = accessTokenService.GetMemeberByToken(model.Token);
            var userWidget = currentMember.GetValue<string>("userWidget");
            var Jarray = JArray.Parse(userWidget);

            foreach (var item in Jarray)
            {
                if (item["widgetId"] != null)
                {

                    if (item["widgetId"].ToString() == model.Id.ToString())
                    {
                        var jsonItems = new List<Dictionary<string, object>>();
                        var items = item["links"].ToString();
                        // if there is no links. Then Create the list.
                        if (items == null)
                        {
                                jsonItems = new List<Dictionary<string, object>>();
                        }
                        else
                        {
                            //If there is links. Then use this list
                            jsonItems = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(items);
                        }
                        //Add the new link
                        jsonItems.Add(new Dictionary<string, object>() {
                            {
                                "title",
                                model.Title
                            },
                            {
                                "link",
                                model.Link
                            },
                            {
                                "tabNumber",
                                model.TabNumber
                            }
                        });

                        var str = JsonConvert.SerializeObject(jsonItems);

                        item["links"].Replace(str);
                        try
                        {
                            currentMember.SetValue("userWidget", JsonConvert.SerializeObject(Jarray));
                            ms.Save(currentMember);
                            status.Success = true;
                        }
                        // if it fails. 
                        catch (Exception ex)
                        {   
                            status.Success = false;
                            throw new Exception("Unable to add  link " + ex.Message);
                        }
                    }
                }
            }
            return status;

        }

        [HttpPost]
        public object WidgetIsOpenOrSelected(Data model)
        {
            Status status = new Status();

            var ms = ApplicationContext.Services.MemberService;
            var currentMember = accessTokenService.GetMemeberByToken(model.Token);
            var allHistory = currentMember.GetValue("userWidget");
            var Jarray = JArray.Parse(allHistory.ToString());

            foreach (var item in Jarray)
            {
                if (item["widgetId"] != null)
                {
                    if (item["widgetId"].ToString() == model.Id.ToString())
                    {
                        if (model.Open != null) {
                            item["open"] = model.Open;
                        }
                        if (model.Selected != null)
                        {
                            item["selected"] = model.Selected;
                        }
                        if (model.TabNumber.ToString() != null && model.TabNumber != 0)
                        {
                            item["tab"] = model.TabNumber.ToString();
                        }
                        try
                        {
                            currentMember.SetValue("userWidget", JsonConvert.SerializeObject(Jarray));
                            ms.Save(currentMember);
                            status.Success = true;
                            status.Msg = "Tab:" + model.TabNumber;
                        }
                        catch (Exception ex)
                        {
                            status.Success = false;
                            status.Msg = ex.ToString();
                            throw new Exception("Unable to update Position " + ex);
                        }

                    }
                }
            }
            return status;
        }
    }
}