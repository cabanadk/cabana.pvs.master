﻿using MitPvs.Feature.Managers;
using MitPvs.Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    [Route("api/[controller]")]
    public class UpdateUserDataController:UmbracoApiController
    {
        private IAccessTokenManager accessTokenService = new AccessTokenManager();

        [HttpPost]
        public object UpdateData(PvsMember member)
        {
            var memberService = Services.MemberService;
            var currentMember = accessTokenService.GetMemeberByToken(member.Token);

            currentMember.SetValue("pvsMemberFirstName", member.FirstName);
            currentMember.SetValue("pvsMemberLastName", member.LastName);
            currentMember.SetValue("pvsMemberTelephoneNumber", member.Number);
            currentMember.SetValue("pvsAlternativeEmail", member.AltEmail);
            currentMember.Username = member.Email;
            currentMember.Name = member.FirstName + " " + member.LastName;
            currentMember.Email = member.Email;

            memberService.Save(currentMember);

            Status status = new Status
            {
                Success = true,
                Msg = currentMember.Name
            };

            return status;
        }
    }
}