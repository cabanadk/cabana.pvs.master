﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    [Route("api/[controller]")]
    public class DictionaryController : UmbracoApiController
    {

        private readonly UmbracoHelper _umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
        [HttpGet]
        public object GetDictionaryItems(string culture)
        {
            //For now, we're only going to get the English items|
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            //var dictionary = new List<DictionaryItem>();

            var root = ApplicationContext.Services.LocalizationService.GetRootDictionaryItems();

            foreach (var item in root)
            {
                dictionary.Add(item.ItemKey, item.Translations.Where(x => x.Language.CultureInfo.ToString() == culture).Select(x => x.Value).FirstOrDefault());
                var level2 = Services.LocalizationService.GetDictionaryItemChildren(item.Key);
                if (level2.Any())
                {
                    foreach (var leveltwo in level2)
                    {
                        dictionary.Add(leveltwo.ItemKey, leveltwo.Translations.Where(x => x.Language.CultureInfo.ToString() == culture).Select(x => x.Value).FirstOrDefault());
                        var level3 = Services.LocalizationService.GetDictionaryItemChildren(leveltwo.Key);
                        if (level3.Any())
                        {
                            foreach (var levelthree in level3)
                            {
                                dictionary.Add(levelthree.ItemKey, levelthree.Translations.Where(x => x.Language.CultureInfo.ToString() == culture).Select(x => x.Value).FirstOrDefault());
                                var level4 = Services.LocalizationService.GetDictionaryItemChildren(levelthree.Key);
                                if (level4.Any())
                                {
                                    foreach (var levelfour in level4)
                                    {
                                        dictionary.Add(levelfour.ItemKey, levelfour.Translations.Where(x => x.Language.CultureInfo.ToString() == culture).Select(x => x.Value).FirstOrDefault());
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return dictionary;
        }
    }
}