﻿using MitPvs.Website.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    public class BasicController : UmbracoApiController
    {
        // GET: GetGuide
        [HttpGet]
        // get all widget data from Umbraco, that are seleted
        public object GetMainGuide(string pageId)
        {
            Status status = new Status();
            var contentService = Services.ContentService;

            var root = contentService.GetById(Int32.Parse(pageId));
            var widgetPage = root.Descendants().Where(x => x.ContentType.Alias.Equals("widgetsPage")).FirstOrDefault();

            var items = widgetPage.GetValue<string>("guide");

            JavaScriptSerializer s = new JavaScriptSerializer();
            var guidePage = s.Deserialize<List<GuidePage>>(items);
            

            return guidePage;
        }
        public Settings GetSettings()
        {
            Settings settings = new Settings();
            var contentService = Services.ContentService;

            var root = contentService.GetById(Umbraco.TypedContentAtRoot().First().Id);
            var widgetPage = root.Descendants().Where(x => x.ContentType.Alias.Equals("widgetsPage")).FirstOrDefault();

            var bgImageId = widgetPage.GetValue("backgroundImage");
            var udi = Udi.Parse(bgImageId.ToString());

            var media = Umbraco.TypedMedia(udi);
            if (media != null)
            {
                settings.BackgroundImage = media.Url;
            }

            return settings;
        }

    }
    public class Settings
    {
        public string BackgroundImage { get; set; }

    }
    public class GuidePage
    {
        public  string Title { get; set; }
        
        public string Items { get; set; }
    }
    
}