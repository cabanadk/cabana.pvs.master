﻿using System.Web.Mvc;
using Umbraco.Web.WebApi;
using System.Net.Mail;
using RestSharp;
using Newtonsoft.Json;

namespace MitPvs.Website.Controllers.api
{
    public class CvrRequest
    {
        public string _source { get; set; }
        public int Size { get; set; }
        

    }
    [Route("api/[controller]")]
    public class TestController : UmbracoApiController
    {
        [System.Web.Http.HttpPost]
        public object CvrData(CvrRequest cvrrequest)
        {

            var client = new RestClient("http://distribution.virk.dk/cvr-permanent/virksomhed/_search");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic Q2FiYW5hXzJfQ1ZSX0lfU0tZRU46YzBiZDRmNjEtNDQxNC00MTExLWExZDgtMzQzMWQ2OWNiZTJj");
            request.AddParameter("application/json", JsonConvert.SerializeObject(cvrrequest), ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);


            return JsonConvert.DeserializeObject(response.Content);
        }
    }
}