﻿using RestSharp;
using Umbraco.Web.WebApi;
using Newtonsoft.Json.Linq;
using System.Web.Http;

namespace MitPvs.Website.Controllers.api
{
    [Route("api/[controller]")]
    public class CvrController : UmbracoApiController
    {
        // GET: GetGuide
        [HttpGet]
        // get cvr data
        public CvrData GetcvrData(string company, int size)
        {
            CvrData cvrData = new CvrData();

            var client = new RestClient("http://distribution.virk.dk/cvr-permanent/virksomhed/_search");
            var request = new RestRequest(Method.POST);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/json");
            request.AddHeader("authorization", "Basic Q2FiYW5hXzJfQ1ZSX0lfU0tZRU46YzBiZDRmNjEtNDQxNC00MTExLWExZDgtMzQzMWQ2OWNiZTJj");
            request.AddParameter("application/json", "{\n  \"_source\":[\n    \"Vrvirksomhed.cvrNummer\",\n    \"Vrvirksomhed.virksomhedMetadata.nyesteNavn.navn\"\n  ],\n  \"query\":{\n    \"query_string\":{\n      \"default_field\":\"Vrvirksomhed.virksomhedMetadata.nyesteNavn.navn\",\n      \"query\":\""+ company + "\"\n    }\n  },\n  \"size\":"+size+"\n}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            cvrData.Status = response.IsSuccessful;
            cvrData.Desc = response.StatusDescription;
            cvrData.Content = JObject.Parse(response.Content);

            return cvrData;
        }
    }

    public class CvrData
    {
        public bool Status { get; set; }
        public string Desc { get; set; }
        public JObject Content { get; set; }
    }
}