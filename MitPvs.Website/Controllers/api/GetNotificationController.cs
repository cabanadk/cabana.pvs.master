﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    public class Notification
    {
        public string Icon { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
    }
    public class GetNotificationController : UmbracoApiController
    {

        // GET: Notification
        // Format Umbraco data to Json
        [HttpGet]
        public object GetNotificationsData(string token, string pageId)
        {
            Collection<Notification> AllNotification = new Collection<Notification>();

            var contentService = Services.ContentService;

            var root = contentService.GetById(Int32.Parse(pageId));
            var notifications = root.Descendants().Where(x => x.ContentType.Alias.Equals("notificationFolder")).FirstOrDefault().Children();

            foreach (var item in notifications.Where(x => x.Published))
            {
                Notification notification = new Notification
                {
                    Icon = item.GetValue<string>("icon"),
                    Title = item.GetValue<string>("title"),
                    Description = item.GetValue<string>("description"),
                    Id = item.Id.ToString()
                };
                
                AllNotification.Add(notification);
            }
            return AllNotification;
        }
    }
}