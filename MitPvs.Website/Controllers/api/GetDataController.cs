﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Umbraco.Web.WebApi;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using MitPvs.Feature.Managers;
using System.Web;

namespace MitPvs.Website.Controllers.api
{
    [Route("api/[controller]")]
    public class GetDataController : UmbracoApiController
    {
        private IMemberService memberService = ApplicationContext.Current.Services.MemberService;
        private IAccessTokenManager accessTokenService = new AccessTokenManager();

        // Format Umbraco data to Json
        [HttpGet]
        public object GetWidgetsData(string token, string amount, string pageId)
        {
            var contentService = Services.ContentService;
            var ms = Services.MemberService;

            var root = contentService.GetById(Int32.Parse(pageId));
            var widgetPage = root.Descendants().Where(x => x.ContentType.Alias.Equals("widgetsPage")).FirstOrDefault();
            var rawWidgets = contentService.GetById(widgetPage.Id).Children().Where(x => x.GetValue<bool>("widgetSelected"));
            // TODO widgets needs a type @Belak
            switch (amount)
            {
                case "all":
                    rawWidgets = contentService.GetById(widgetPage.Id).Children();
                    break;
                case "selected":
                    rawWidgets = contentService.GetById(widgetPage.Id).Children().Where(x => x.GetValue<bool>("widgetSelected"));
                    break;
                default:
                    rawWidgets = contentService.GetById(widgetPage.Id).Children().Where(x => x.GetValue<bool>("widgetSelected"));
                    break;
            }

            var member = accessTokenService.GetMemeberByToken(token);
            

            // All user widget items
            var UserStoredWidgetData = "";
            if (member != null)
            {
                UserStoredWidgetData = member.GetValue<string>("userWidget");
            } else
            {
                return false;
            }

            JavaScriptSerializer s = new JavaScriptSerializer();
            UserWidgetData userData = new UserWidgetData();
            var userWidgetsList = s.Deserialize<List<UserWidgetData>>(UserStoredWidgetData);

            var widgets = new List<object>();
            
            foreach (var widget in rawWidgets)
            {

                var widgetData = new Dictionary<string, object>();


                widgetData.Add("WidgetId", widget.Id);
                if (userWidgetsList != null)
                {
                    var CurrentWidget = userWidgetsList.Where(x => x.WidgetId == widget.Id).FirstOrDefault();
                    widgetData.Add("UserLinks", CurrentWidget.Links);
                    widgetData.Add("UserSelected", CurrentWidget.Selected);
                    widgetData.Add("Open", CurrentWidget.Open);
                    widgetData.Add("Tab", CurrentWidget.Tab);
                }

                foreach (var value in widget.Properties)
                {
                    if (value.Alias == "pvsMemberRightArea")
                    {
                        List<string> checkboxeValues = new List<string>();
                        foreach (var item in value.Value.ToString().Split(',')) {
                            var preValues = Umbraco.GetPreValueAsString(Int32.Parse(item));
                            checkboxeValues.Add(preValues);  
                        }
                        widgetData.Add(value.Alias, checkboxeValues);

                    }
                    else
                    {
                        widgetData.Add(value.Alias, value.Value);

                    }
                }
                widgets.Add(widgetData);
            }

            return widgets;

        }
    }
    class UserWidgetData
    {
        public int WidgetId { get; set; }

        public string Selected { get; set; }
        public string Links { get; set; }
        public string Tab { get; set; }
        public string Open { get; set; }
    }
}