﻿using MitPvs.Feature.Managers;
using MitPvs.Website.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Core.Models;
using Umbraco.Web.WebApi;

namespace MitPvs.Website.Controllers.api
{
    public class ShortcutController : UmbracoApiController
    {

        private IAccessTokenManager accessTokenService = new AccessTokenManager();

        [System.Web.Http.HttpPost]
        public Status Add(List<Shortcut> shortcut)
        {
            Status status = new Status();
            var token = shortcut.FirstOrDefault().Token;
            var WidgetId = shortcut.FirstOrDefault().WidgetId;

            var memberService = Services.MemberService;
            var currentMember = accessTokenService.GetMemeberByToken(token);


            // All user widget items
            var items = currentMember.GetValue<string>("userWidget");
            JavaScriptSerializer s = new JavaScriptSerializer();
            var widgets = s.Deserialize<List<UserWidget>>(items);

            // Single item
            var shortcutWidget = widgets.Where(x => x.widgetId.ToString() == WidgetId.ToString()).FirstOrDefault();
            var links = shortcutWidget.links;
            var jsonItems = new List<Dictionary<string, object>>();

            if (links == null)
            {
                jsonItems = new List<Dictionary<string, object>>();
            }
            else
            {
                jsonItems = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(links);
            }

            foreach (var item in shortcut)
            {
                //Random ID
                //TODO ADD Time code
                jsonItems.Add(new Dictionary<string, object>() {
                    {
                    "title",
                        item.Title
                    },
                    {
                    "link",
                        item.Link
                    },
                    {
                    "order",
                        (item.Order == null) ? "99" : item.Order
                    },
                    {
                    "tabNumber",
                        (item.TabNumber == null) ? 1 : Int32.Parse(item.TabNumber)
                    },
                    {
                    "linkId",
                        item.LinkId
                    },
                    {
                    "pvsLink",
                        item.PvsLink
                    },
                    {
                    "showLink",
                        item.ShowLink
                    }
                });
            }

            widgets.FirstOrDefault(x => x.widgetId == WidgetId.ToString()).links = JsonConvert.SerializeObject(jsonItems);
            var updatedWidgets = JsonConvert.SerializeObject(widgets);
            currentMember.SetValue("userWidget", updatedWidgets);

            memberService.Save(currentMember);
            status.Success = true;
            status.Msg = "Link: " + shortcut + " added;";
            return status;
        }


        [System.Web.Http.HttpPost]
        public Status Remove(Shortcut shortcut)
        {
            Status status = new Status();
            var memberService = Services.MemberService;
            var currentMember = accessTokenService.GetMemeberByToken(shortcut.Token);

            // All user widget items
            var items = currentMember.GetValue<string>("userWidget");

            JavaScriptSerializer s = new JavaScriptSerializer();
            var widgets = s.Deserialize<List<UserWidget>>(items);

            //// Single item
            var shortcutWidget = widgets.Where(x => x.widgetId.ToString() == shortcut.WidgetId.ToString()).FirstOrDefault();
            var links = shortcutWidget.links;

            var linksitems = (links == null) ? new List<Dictionary<string, object>>() : JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(links);
            // Find the link in dic
            var item = linksitems.FirstOrDefault(x => x.ContainsValue(shortcut.LinkId));
            linksitems.Remove(item);


            widgets.FirstOrDefault(x => x.widgetId == shortcut.WidgetId.ToString()).links = JsonConvert.SerializeObject(linksitems);
            var updatedWidgets = JsonConvert.SerializeObject(widgets);
            //Save the data back on the user
            currentMember.SetValue("userWidget", updatedWidgets);
            memberService.Save(currentMember);
            status.Success = true;
            status.Msg = "Link deleted: " + shortcut.Title + " on member: " + currentMember.Name;
            return status;
        }


        [System.Web.Http.HttpPost]
        public Status Update(List<Shortcut> shortcut)
        {
            Status status = new Status();
            var token = shortcut.FirstOrDefault().Token;
            var WidgetId = shortcut.FirstOrDefault().WidgetId;

            var memberService = Services.MemberService;
            var currentMember = accessTokenService.GetMemeberByToken(token);

            // All user widget items
            var items = currentMember.GetValue<string>("userWidget");
            JavaScriptSerializer s = new JavaScriptSerializer();
            var widgets = s.Deserialize<List<UserWidget>>(items);

            // Single item
            var shortcutWidget = widgets.Where(x => x.widgetId.ToString() == WidgetId.ToString()).FirstOrDefault();
            var links = shortcutWidget.links;
            var linksitems = (links == null) ? new List<Dictionary<string, object>>() : JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(links);
            // Find the link in dic
            foreach (var link in shortcut)
            {
                var item = linksitems.FirstOrDefault(x => x.ContainsValue(link.LinkId));

                if (link.Order != null) { item["order"] = link.Order; }
                if (link.Title != null) { item["title"] = link.Title; }
                if (link.Link != null) { item["link"] = link.Link; }
                if (link.ShowLink != null) { item["showLink"] = Int32.Parse(link.ShowLink); }
                if (link.PvsLink != null) { item["pvsLink"] = Int32.Parse(link.PvsLink); }
            }
            
            
            widgets.FirstOrDefault(x => x.widgetId == WidgetId.ToString()).links = JsonConvert.SerializeObject(linksitems);
            var updatedWidgets = JsonConvert.SerializeObject(widgets);
            currentMember.SetValue("userWidget", updatedWidgets);

            memberService.Save(currentMember);
            status.Success = true;
            status.Msg = "Link: " + shortcut.FirstOrDefault().WidgetId + " updated;";
            return status;

        }
    }
}
