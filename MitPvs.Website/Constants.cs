﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitPvs.Website
{
    public struct Constants
    {
        public struct PageBase
        {
            public struct FieldNames
            {
                public static readonly string TitleField = "mainTitle";
                public static readonly string AbstractField = "abstract";
            }
        }

        public struct PageBody
        {
            public struct FieldNames
            {
                public static readonly string BodyField = "body";
            }
        }
    }
}