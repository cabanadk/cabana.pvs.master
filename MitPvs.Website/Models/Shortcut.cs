﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitPvs.Website.Models
{
    public class Shortcut
    {
        public string TabNumber { get; set; }
        public string Order { get; set; }
        public string Title { get; set; }
        public string Link { get; set; }
        public string Token { get; set; }
        public string WidgetId { get; set; }
        public string LinkId { get; set; }
        public string PvsLink { get; set; }
        public string ShowLink { get; set; }

    }
}