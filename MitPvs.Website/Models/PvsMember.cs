﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitPvs.Website.Models
{
    public class PvsMember
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string AltEmail { get; set; }
        public string Number { get; set; }
        public string Token { get; set; }
    }
}