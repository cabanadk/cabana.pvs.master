﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitPvs.Website.Models
{
    public class Status
    {
        public bool Success { get; set; }
        public string Msg { get; set; }
        public int Id { get; set; }
    }
}