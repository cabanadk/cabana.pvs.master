﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitPvs.Website.Models
{
    public class UserWidget
    {

        public string widgetName { get; set; }
        public string widgetId { get; set; }
        public string links { get; set; }
        public string open { get; set; }
        public string selected { get; set; }
        public string tab { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string ncContentTypeAlias { get; set; }
    }
}