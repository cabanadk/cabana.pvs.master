﻿using Umbraco.Core;
using Umbraco.Core.Services;
using Newtonsoft.Json;
using MitPvs.Website.Controllers.api;
using System;
using Umbraco.Core.Events;
using Umbraco.Core.Models;

namespace MitPvs.Website.EventHandlers
{
    public class GetDictionaryItems : ApplicationEventHandler
    {
        public new void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            LocalizationService.SavedDictionaryItem += SavedDictionaryItem;
        }

        private void SavedDictionaryItem(ILocalizationService sv, SaveEventArgs<IDictionaryItem> args)
        {
            var test = sv;
            var test2 = args;
        }


        //private void ContentService_Saved(IContentService sender, Umbraco.Core.Events.SaveEventArgs<Umbraco.Core.Models.IContent> e)
        //{
        //    string ENJsonFile = @"C:\Development\cabana.pvs.master\MitPvs.Website\en-US_dic.json";
        //    string DKJsonFile = @"C:\Development\cabana.pvs.master\MitPvs.Website\da-DK_dic.json";


        //    foreach (var item in e.SavedEntities)
        //    {
        //        if (item.Level == 1)
        //        {
        //            var id = item.Id;
        //            var test = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
        //            //Get Data From Api Controller
        //            var ENData = new DictionaryController().GetDictionaryItems("en-US");
        //            var DKData = new DictionaryController().GetDictionaryItems("da-DK");
        //            //To Json                
        //            var ENJson = JsonConvert.SerializeObject(ENData);
        //            var DKJson = JsonConvert.SerializeObject(DKData);
        //            //Rewrite Old file
        //            System.IO.File.WriteAllText(ENJsonFile, ENJson);
        //            System.IO.File.WriteAllText(DKJsonFile, DKJson);
        //        }
        //    }
        //}
    }
}