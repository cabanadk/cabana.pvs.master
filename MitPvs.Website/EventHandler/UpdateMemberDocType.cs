﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Models.EntityBase;
using Umbraco.Core.Services;

namespace MitPvs.Website.EventHandler
{
    public class UpdateMemberDocType : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Published += ContentService_Published;
            //MemberService.Saving += MemberService_Saving;
            ContentService.Deleting += ContentService_Deleted;
        }
        //TOD remove widget on user when deleted from pvs list
        private void ContentService_Deleted(IContentService sender, DeleteEventArgs<IContent> e)
        {
            var cs = ApplicationContext.Current.Services.ContentService;
            var ms = ApplicationContext.Current.Services.MemberService;
            foreach (var widget in e.DeletedEntities)
            {
                var parentalias = cs.GetById(widget.Id).Parent();
                var isAWidget = (parentalias != null && parentalias.ContentType.Alias == "widgetsPage") ? true : false;

                if (isAWidget)
                {
                    var test = "Delete";
                }
            }
        }

        private static void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            var cs = ApplicationContext.Current.Services.ContentService;
            var ms = ApplicationContext.Current.Services.MemberService;

            foreach (var widget in e.PublishedEntities)
            {
                var parentalias = cs.GetById(widget.Id).Parent();
                var isAWidget = (parentalias != null && parentalias.ContentType.Alias == "widgetsPage") ? true : false;

                if (isAWidget)
                {

                    var Members = ApplicationContext.Current.Services.MemberService.GetAllMembers();
                    var name = cs.GetById(widget.Id).Name;

                    foreach (var member in Members)
                    {

                        foreach (var propety in member.Properties.Where(x => x.Alias == "userWidget"))
                        {
                            var value = propety.Value;
                            var userWidget = (value == null) ? new List<Dictionary<string, object>>() : JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(value.ToString());

                            var widgetExitst = userWidget.SelectMany(x => x).Where(x => (x.Value == null ? "": x.Value.ToString()) == widget.Id.ToString()).FirstOrDefault();

                            if (widgetExitst.Value == null)
                            {
                                userWidget.Add(new Dictionary<string, object>() {
                                    {
                                        "widgetName",name
                                    },
                                    {
                                        "widgetId",widget.Id
                                    },
                                    {
                                        "selected", 1
                                    },
                                    {
                                        "open", 1
                                    }
                                });

                                member.SetValue("userWidget", JsonConvert.SerializeObject(userWidget));
                                ms.Save(member);
                            }
                        }
                    }
                }
            }
        }
    }
}