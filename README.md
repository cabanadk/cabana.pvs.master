﻿# MitPVS

Dashboard for pvs members

## Getting Started

### Installing
* Restore database with the **.bak** file in **/Databases** folder
* IIS points to **/MitPvs.Website**

* Build the solution

#### Umbraco setup 
##### Login
**Username**: admin@cabana.dk
**Password**: Cabana20!8

* Run ```npm install``` in
	* **MitPvs.Website.Frontend**
	* **MitPvs.Vue**
* Make sure nuget packages in VS project are installed  
* Make Sure webconfig points to the correct database


#### Vue setup


##### Local setup


* Run ```npm run dev``` in **/MitPvs.Vue - This will start up the server

##### Global setup
 
* Run ```npm run build``` in **/MitPvs.Vue** - This will build and copy the files to **/MitPvs.Website/dist**


#### Frontend Setup
* Make sure task runner are running. Should start ```gulp watch``` when the Solution opens.
* When ```gulp watch```are running it will automatic update the *style* and *js* files to **MitPvs.Website**

## Deployment

Checklist:
* Run ```npm run build``` in **/MitPvs.Vue** - This will build and copy the files to **/MitPvs.Website/dist**

Now ready for deploy !

## Authors

* **Thomas Hansen** - *Developer* - Tlf: [link](tel:31444252 "31444251") - Mail: [link](mailto:tha@cabana.dk tha@cabana.dk


