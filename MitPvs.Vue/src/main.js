import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import AddWidget from "./components/AddWidget.vue";
import UserProfile from "./components/UserProfile.vue";
import WidgetFrame from "./components/WidgetFrame.vue";
import VModal from "vue-js-modal";
var SocialSharing = require("vue-social-sharing");

// Require dependencies
var VueCookie = require("vue-cookie");

// detect what device we are on
var MobileDetect = require("mobile-detect");
var md = new MobileDetect(window.navigator.userAgent);
// Tell Vue to use the plugin
Vue.use(VueCookie);
Vue.use(SocialSharing);
Vue.use(VueRouter);
Vue.use(VModal, {
    dialog: true,
    dynamic: true
});


window.axios = require("axios");
window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
Vue.prototype.$http = window.axios;


// MENU ROUTES
const routes = [
    {
        path: "/addwidget/:accessToken",
        name: "addwidget",
        component: AddWidget,
        props: true,
        meta: {
            cookies: ['token'], // need to have submitted passcode and made a client
            redirect: '/'
        }
    },
    {
        path: "/user/:accessToken",
        name: "user",
        component: UserProfile,
        props: true,
        meta: {
            cookies: ['token'], // need to have submitted passcode and made a client
            redirect: '/'
        }
    },
    {
        path: "/:accessToken",
        name: "home",
        component: WidgetFrame,
        props: true,
        meta: {
            cookies: ['token'], // need to have submitted passcode and made a client
            redirect: '/'
        },
      beforeEnter: (to, from, next) => {
        // Build dic when widget are loaded
        if (from.name === null) {
            var lang = document.getElementById("pageId").dataset.lang;
            Vue.prototype.$http.get(
              Vue.prototype.$localUrl +
              "/umbraco/api/Dictionary/GetDictionaryItems?culture=" + lang
            )
              .then(response => {
                localStorage.setItem('dic', JSON.stringify(response.data));
                next()

              })
              .catch(e => {
                console.log("Failed getting the dic data");
                console.log(e);
              });
          } else {
            next()
          }
        }
    }
   
];
// history removes the #
const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {
    if (to.meta.cookies === undefined) {
        if (Vue.cookie.get("userToken") !== null) {
            next({
                name: 'home', params: { accessToken: Vue.cookie.get("userToken") }
            });
        } else {
            next({ name: 'root' });
        }
    }
    else {
        next();
    }
});


// Uses this to get data from the right lang.
Vue.prototype.$pageId = document.getElementById("pageId").dataset.currentpageid;

//Global API URL - defined in package.json
Vue.prototype.$localUrl = VUE_APP_APIURL;

Vue.filter("capitalize", function(value) {
    if (!value) return "";
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
});

// Format the date
import moment from "moment";
Vue.filter("formatDate", function(value) {
    if (value) {
      return moment(String(value)).format("DD.MM.YYYY");
    }
});

Vue.filter("formatDateTime", function (value) {
  if (value) {
    return moment(String(value)).format("DD.MM.YYYY, H:MM");
  }
});
Vue.filter("formatTime", function (value) {
  if (value) {
    return moment(String(value)).format("H:MM");
  }
});

import optionsMixin from "./mixins/mixins.js";


new Vue({
    el: "#app",
    router: router,
    render: h => h(App),
    components: { App, AddWidget, UserProfile, WidgetFrame },
    mixins: [optionsMixin],
    data() {
        return {
    };
  }, mounted() {
      window.addEventListener('orientationchange', this.screenChange);
  },created() {
      this.screenChange();
  },
  methods: {
    screenChange: function () {
      setTimeout(function () {
        // Check if it's a mobile
          if (md.mobile() && md.tablet() === null) {
            document.body.classList.add("mobile-frame");
            document.getElementById("mobile-frame").style.display = "flex";
            document.getElementById("app").style.display = "none";
            document.getElementById("ipad-frame").style.display = "none";

          }
          // or a tablet in portrait mode
          else if (md.tablet != null && window.innerHeight > window.innerWidth) {
            document.body.classList.add("ipad-frame");
            document.getElementById("ipad-frame").style.display = "flex";
            document.getElementById("mobile-frame").style.display = "none";
            document.getElementById("app").style.display = "none";
          }
          // or not a mobile or a tablet in portrait mode
          else {
            document.getElementById("app").style.display = "block";
            document.getElementById("mobile-frame").style.display = "none";
            document.getElementById("ipad-frame").style.display = "none";
            document.body.classList.remove("ipad-frame");
            document.body.classList.remove("mobile-frame");
          }
        }, 500);
      
      }
  }
});
