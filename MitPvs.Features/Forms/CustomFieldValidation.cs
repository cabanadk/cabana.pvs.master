﻿using System.Linq;
using System.Web.Mvc;
using Umbraco.Core;
using Umbraco.Core.Services;
using Umbraco.Forms.Mvc;
using Umbraco.Web;

namespace MitPvs.Feature.Forms
{
    public class CustomFieldValidation : ApplicationEventHandler
    {
        private readonly IMemberService memberService = ApplicationContext.Current.Services.MemberService;

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Forms.Web.Controllers.UmbracoFormsController.FormValidate += FormRenderController_FormValidate;
        }

        void FormRenderController_FormValidate(object sender, Umbraco.Forms.Mvc.FormValidationEventArgs e)
        {
            if (e.Form.Name == "Create Profile")
            {
                var s = sender as Controller;
                if (s != null)
                {
                    ValidateEmail(s, e);
                    ValidateAlternativeEmail(s, e);
                }
            }
        }

        void ValidateEmail(Controller s, FormValidationEventArgs e)
        {
            var emailField = e.Form.AllFields.SingleOrDefault(f => f.Alias == "signUpEmail");
            if (emailField == null)
            {
                return;
            }

            var emailFieldKey = emailField.Id.ToString();
            var email = e.Context.Request[emailFieldKey];

            var umbracoHelper = new UmbracoHelper();

            if (memberService.Exists(email))
            {
                s.ModelState.AddModelError(emailFieldKey, umbracoHelper.GetDictionaryValue("Email exists message"));
            }

            var confirmEmailField = e.Form.AllFields.SingleOrDefault(f => f.Alias == "signUpConfirmEmail");
            if (confirmEmailField == null)
            {
                return;
            }

            var confirmEmailFieldKey = confirmEmailField.Id.ToString();
            var confirmEmail = e.Context.Request[confirmEmailFieldKey];

            if (email != confirmEmail)
            {
                s.ModelState.AddModelError(confirmEmailFieldKey, umbracoHelper.GetDictionaryValue("Email not match message"));
            }
        }

        void ValidateAlternativeEmail(Controller s, FormValidationEventArgs e)
        {
            var emailField = e.Form.AllFields.SingleOrDefault(f => f.Alias == "alternativeEmail");
            var confirmEmailField = e.Form.AllFields.SingleOrDefault(f => f.Alias == "confirmAlternativeEmail");

            if (emailField == null || confirmEmailField == null)
            {
                return;
            }

            var emailFieldKey = emailField.Id.ToString();
            var email = e.Context.Request[emailFieldKey];
            var confirmEmailFieldKey = confirmEmailField.Id.ToString();
            var confirmEmail = e.Context.Request[confirmEmailFieldKey];

            var umbracoHelper = new UmbracoHelper();

            if (email != confirmEmail)
            {
                s.ModelState.AddModelError(confirmEmailFieldKey, umbracoHelper.GetDictionaryValue("Email not match message"));
            }
        }
    }
}