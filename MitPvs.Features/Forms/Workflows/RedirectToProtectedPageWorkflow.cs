﻿using MitPvs.Feature.AccessToken;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Enums;

namespace MitPvs.Feature.Forms.Workflows
{
    public class RedirectToProtectedPageWorkflow : WorkflowType
    {
        private IMemberService memberService = ApplicationContext.Current.Services.MemberService;

        [Umbraco.Forms.Core.Attributes.Setting("Go to page", description = "Page redirect to", view = "Pickers.Content")]
        public string GotoPage { get; set; }
        public RedirectToProtectedPageWorkflow()
        {
            Id = new Guid("7c8e492e-0bf1-471f-a696-d8d8927e7d54");
            Name = "Redirect to a protected content page workflow";
            Description = "Redirect to a protected content page";
            Icon = "icon-mindmap";
        }

        public override WorkflowExecutionStatus Execute(Record record, RecordEventArgs e)
        {
            try
            {
                LogHelper.Info(this.GetType(), "Execute RedirectToProtectedPageWorkflow.");

                // get the correct values from the form
                var email = record.GetRecordField("Email").ValuesAsString();

                var token = GetAccessTokenByEmail(email);

                var gotoPageUrl = $"{umbraco.library.NiceUrl(Convert.ToInt32(GotoPage)).TrimEnd("/")}?accessToken={token}";
                HttpContext.Current.Response.Redirect(gotoPageUrl);

                return WorkflowExecutionStatus.Completed;
            }
            catch(InvalidOperationException)
            {
                var token = HttpContext.Current.Request.QueryString["accessToken"];

                var gotoPageUrl = $"{umbraco.library.NiceUrl(Convert.ToInt32(GotoPage)).TrimEnd("/")}?accessToken={token}";
                HttpContext.Current.Response.Redirect(gotoPageUrl);
                return WorkflowExecutionStatus.Completed;
            }
            catch (Exception ex)
            {
                LogHelper.WarnWithException(this.GetType(), string.Format("Unable to redirect to protected page: {0}", ex.Message), ex);
                return WorkflowExecutionStatus.Failed;
            }
        }

        public override List<Exception> ValidateSettings()
        {
            var errors = new List<Exception>();
            return errors;
        }

        private string GetAccessTokenByEmail(string email)
        {

            // check we have data for all required fields
            if (string.IsNullOrEmpty(email))
            {
                LogHelper.Warn(this.GetType(), string.Format("Unable to redirect to protected page: Missing email."));
                return string.Empty;
            }

            if (!memberService.Exists(email))
            {
                LogHelper.Warn(this.GetType(), string.Format("Unable to redirect to protected page: Member not existed."));
                return string.Empty;
            }

            var member = memberService.GetByUsername(email);
            return member.GetValue(Constants.PvsMember.AccessTokenProperty).ToString();
        }
    }
}