﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Umbraco.Core;
using Umbraco.Core.Logging;
using Umbraco.Core.Services;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Enums;

namespace MitPvs.Feature.Forms.Workflows
{
    public class AddAlternativeEmailWorkflow : WorkflowType
    {
        private IMemberService memberService = ApplicationContext.Current.Services.MemberService;

        public AddAlternativeEmailWorkflow()
        {
            Id = new Guid("9d8e492e-0bf1-471f-a696-d8d8927e7d54");
            Name = "Add alternative email";
            Description = "Add an alternative email to PVS member";
            Icon = "icon-message";
        }

        public override WorkflowExecutionStatus Execute(Record record, RecordEventArgs e)
        {
            try
            {
                LogHelper.Info(this.GetType(), "Execute AddAlternativeEmailWorkflow.");

                var loginMember = Membership.GetUser();
                if (loginMember == null)
                {
                    LogHelper.Warn(this.GetType(), string.Format("Unable to add alternative email: No login member."));
                    return WorkflowExecutionStatus.Failed;
                }

                var alternativeEmail = record.GetRecordField("Alternative Email").ValuesAsString();
                var alternativeConfirmEmail = record.GetRecordField("Confirm Alternative Email").ValuesAsString();

                //user click "spring over"
                if (string.IsNullOrEmpty(alternativeEmail)&&string.IsNullOrEmpty(alternativeConfirmEmail))
                {
                    return WorkflowExecutionStatus.Completed;
                }

                // check we have data for all required fields
                if (string.IsNullOrEmpty(alternativeEmail) || !alternativeEmail.Equals(alternativeConfirmEmail, StringComparison.InvariantCultureIgnoreCase))
                {
                    LogHelper.Warn(this.GetType(), string.Format("Unable to add alternative email: email is not identical."));
                    return WorkflowExecutionStatus.Failed;
                }

                if (!memberService.Exists(loginMember.UserName))
                {
                    LogHelper.Warn(this.GetType(), string.Format("Unable to add alternative email: PVS member not found."));
                    return WorkflowExecutionStatus.Failed;
                }

                var member = memberService.GetByUsername(loginMember.UserName); 
                member.SetValue(Constants.PvsMember.AlternativeEmailProperty, alternativeEmail);

                memberService.Save(member);

                return WorkflowExecutionStatus.Completed;
            }
            catch (Exception ex)
            {
                LogHelper.WarnWithException(this.GetType(), string.Format("Unable to add alternative email: {0}", ex.Message), ex);
                return WorkflowExecutionStatus.Failed;
            }
        }

        public override List<Exception> ValidateSettings()
        {
            var errors = new List<Exception>();
            return errors;
        }
    }
}