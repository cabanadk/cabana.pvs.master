﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Core.Logging;
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Enums;
using Umbraco.Web.Security.Providers;
using Umbraco.Forms.Core.Services;
using MitPvs.Feature.AccessToken;
using System.Web.Security;

namespace MitPvs.Feature.Forms.Workflows
{
    public class CreateProfileWorkflow : WorkflowType
    {
        private IMemberService memberService = ApplicationContext.Current.Services.MemberService; 
        private AccessTokenService accessTokenService = new AccessTokenService();

        public CreateProfileWorkflow()
        {
            Id = new Guid("8b8e492e-0bf1-471f-a696-d8d8927e7d54");
            Name = "Create profile workflow";
            Description = "Creates a new profile as Umbraco member";
            Icon = "icon-client";

            //WorkflowService.WorkflowFailed += WorkflowService_WorkflowFailed;
        }

        public override WorkflowExecutionStatus Execute(Record record, RecordEventArgs e)
        {
            try
            {
                LogHelper.Info(this.GetType(), "Execute CreateProfileWorkflow.");

                // get the correct values from the form
                var firstName = record.GetRecordField("Fornavn").ValuesAsString();
                var lastName = record.GetRecordField("Efternavn").ValuesAsString();
                var email = record.GetRecordField("Email").ValuesAsString();
                var confirmEmail = record.GetRecordField("Confirm Email").ValuesAsString();
                var telephoneNumber = record.GetRecordField("Telephone Number").ValuesAsString();
                //var alternativeEmail = record.GetRecordField("Alternative Email").ValuesAsString();
                //var alternativeConfirmEmail = record.GetRecordField("Confirm Alternative Email").ValuesAsString();

                // check we have data for all required fields
                if (string.IsNullOrEmpty(firstName) || string.IsNullOrEmpty(lastName) || 
                    string.IsNullOrEmpty(email) || !email.Equals(confirmEmail, StringComparison.InvariantCultureIgnoreCase)|| 
                    string.IsNullOrEmpty(telephoneNumber))
                {
                    LogHelper.Warn(this.GetType(), string.Format("Unable to create profile: Missing firstName, lastName, email or email is not identical."));
                    return WorkflowExecutionStatus.Failed;
                }

                if(memberService.Exists(email))
                {
                    LogHelper.Warn(this.GetType(), string.Format("Unable to create profile: Email has already been used."));
                    return WorkflowExecutionStatus.Failed;
                }

                var member = memberService.CreateMemberWithIdentity(email, email, $"{firstName} {lastName}", Constants.PvsMember.MemberTypeName);
                member.SetValue(Constants.PvsMember.FirstNameProperty, firstName);
                member.SetValue(Constants.PvsMember.LastNameProperty, lastName);
                member.SetValue(Constants.PvsMember.TelephoneNumberProperty, telephoneNumber);

                member.SetValue(Constants.PvsMember.AccessTokenProperty, accessTokenService.GenerateToken());
                member.SetValue(Constants.PvsMember.AccessTokenExpirationTimeProperty, DateTime.Now.Ticks);

                memberService.AssignRole(member.Id, Constants.PvsMember.PvsMemberGroupName);
                memberService.Save(member);
                FormsAuthentication.SetAuthCookie(member.Username, false);

                return WorkflowExecutionStatus.Completed;
            }
            catch (Exception ex)
            {
                LogHelper.WarnWithException(this.GetType(), string.Format("Unable to create profile: {0}", ex.Message), ex);
                return WorkflowExecutionStatus.Failed;
            }
        }

        public override List<Exception> ValidateSettings()
        {
            var errors = new List<Exception>();
            return errors;
        }


        //private void WorkflowService_WorkflowFailed(object sender, Umbraco.Forms.Core.WorkflowEventArgs e)
        //{
        //    var t = e.Workflow.Name;
        //}
    }
}