﻿using MitPvs.Feature.Managers;
using MitPvs.Feature.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using umbraco.NodeFactory;

namespace MitPvs.Feature.Repositories
{
    public class PvsMemberRepository : IPvsMemberRepository
    {
        private IMemberService memberService = ApplicationContext.Current.Services.MemberService;
        private IAccessTokenManager accessTokenService = new AccessTokenManager();

        public string CreateMemeberAndGetToken(PvsMemberModel data)
        {
            if (memberService.Exists(data.Email))
            {
                return string.Empty;
            }

            var member = memberService.CreateMemberWithIdentity(data.Email, data.Email, $"{data.FirstName} {data.LastName}", Constants.PvsMember.MemberTypeName);
            member.SetValue(Constants.PvsMember.FirstNameProperty, data.FirstName);
            member.SetValue(Constants.PvsMember.LastNameProperty, data.LastName);
            //member.SetValue(Constants.PvsMember.TelephoneNumberProperty, data.PhoneNo);

            var token = accessTokenService.GenerateToken();
            member.SetValue(Constants.PvsMember.AccessTokenProperty, token);
            member.SetValue(Constants.PvsMember.AccessTokenExpirationTimeProperty, DateTime.Now.Ticks);

            


            memberService.AssignRole(member.Id, Constants.PvsMember.PvsMemberGroupName);
            memberService.Save(member);

            return token;
        }

        public bool PvsMemberExists(string email)
        {
            return memberService.Exists(email);
        }

        public void UpdateMemberProperty(string token, string properyName, string value)
        {
            var member = accessTokenService.GetMemeberByToken(token);
            if (member != null)
            {
                member.SetValue(properyName, value);
                memberService.Save(member);
            }
        }

        public IMember GetPvsMember(string email)
        {
            return memberService.GetByUsername(email);
        }
    }
}