﻿using MitPvs.Feature;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace MitPvs.Feature.Repositories
{
    public static class MediaRepository
    {
        public static string GetBackgroundImageUrl()
        {
            var currentNode = UmbracoContext.Current.PublishedContentRequest.PublishedContent;
            var backgroundImage = currentNode.GetPropertyValue<IPublishedContent>(Constants.Media.FieldNames.BackgroundImageField);
            if (backgroundImage != null)
            {
                return backgroundImage.Url;
            }

            var homeNode = currentNode.AncestorOrSelf(1);
            backgroundImage = homeNode.GetPropertyValue<IPublishedContent>(Constants.Media.FieldNames.BackgroundImageField);
            if(backgroundImage!=null)
            {
                return backgroundImage.Url;
            }

            return string.Empty;
        }
    }
}