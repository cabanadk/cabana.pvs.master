﻿using MitPvs.Feature.Models;
using Umbraco.Core.Models;

namespace MitPvs.Feature.Repositories
{
    public interface IPvsMemberRepository
    {
        string CreateMemeberAndGetToken(PvsMemberModel data);
        bool PvsMemberExists(string email);
        void UpdateMemberProperty(string token, string properyName, string value);

        IMember GetPvsMember(string email);
    }
}