﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MitPvs.Feature.Managers;
using MitPvs.Feature.Models;
using MitPvs.Feature.Repositories;
using Umbraco.Web.Mvc;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Core.Logging;
using System.Collections.Specialized;
using Umbraco.Core.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MitPvs.Feature.Controllers
{
    public class CreateProfileController : SurfaceController
    {
        private readonly IPvsMemberRepository pvsMemberRepository = new PvsMemberRepository();
        private readonly IAccessTokenManager accessTokenManager = new AccessTokenManager();
        private readonly IEmailManager emailManager = new EmailManager();
        private readonly IUserProfileStateManager userProfileStateManager = new UserProfileStateManager();

        [HttpPost]
        public ActionResult ProfileDetailsStep(ProfileDetailsFormModel model)
        {
            if (ModelState.IsValid)
            {
                if (pvsMemberRepository.PvsMemberExists(model.Email))
                {
                    LogHelper.Error(this.GetType(), "User uses an existing email addres.", new Exception());
                    return RedirectToCurrentUmbracoPage();
                }

                userProfileStateManager.FirstName = model.FirstName;
                userProfileStateManager.LastName = model.LastName;
                userProfileStateManager.Email = model.Email;
                //userProfileStateManager.PhoneNo = model.Phone;

                var nextPage = ((PublishedContentModel)CurrentPage.GetProperty(Constants.FormContent.FieldNames.NextStep).Value).Id;
                userProfileStateManager.AddAllowedStep(nextPage);

                return RedirectToUmbracoPage(nextPage);
            }

            return RedirectToCurrentUmbracoPage();
        }


        [HttpPost]
        public ActionResult AcceptTermsStep(AcceptTermsFormModel model)
        {
            if (!model.Accepted)
            {
                LogHelper.Error(this.GetType(), "User has not accepted the terms.", new Exception());
                return RedirectToCurrentUmbracoPage();
            }

            try
            {
                userProfileStateManager.HasAcceptedTerms = model.Accepted;
                userProfileStateManager.EmailCode = accessTokenManager.Generate4DigitsRandomNo().ToString();
                emailManager.SendConfirmationEmail(userProfileStateManager.Email, userProfileStateManager.EmailCode, userProfileStateManager.FirstName, model.Parentid);

                var nextPage = ((PublishedContentModel)CurrentPage.GetProperty(Constants.FormContent.FieldNames.NextStep).Value).Id;
                userProfileStateManager.AddAllowedStep(nextPage);

                return RedirectToUmbracoPage(nextPage);
            }
            catch (Exception ex)
            {
                LogHelper.Error(this.GetType(), ex.Message, ex);
                return RedirectToCurrentUmbracoPage();
            }

        }

        [HttpPost]
        public ActionResult ConfirmEmailStep(ConfirmEmailFormModel model)
        {
            if (model.EmailCode != userProfileStateManager.EmailCode.ToString() || model.Email.ToLower() != userProfileStateManager.Email.ToLower())
            {
                LogHelper.Error(this.GetType(), "User types incorrect email code or email", new Exception());
                var qs = new NameValueCollection();
                qs.Add("verifyerror", "true");
                return RedirectToCurrentUmbracoPage(qs);
            }

            userProfileStateManager.HasVerified = true;

            userProfileStateManager.AccessToken = pvsMemberRepository.CreateMemeberAndGetToken(new PvsMemberModel { Email = userProfileStateManager.Email, FirstName = userProfileStateManager.FirstName, LastName = userProfileStateManager.LastName });
            //userProfileStateManager.AccessToken = pvsMemberRepository.CreateMemeberAndGetToken(new PvsMemberModel { Email = userProfileStateManager.Email, FirstName = userProfileStateManager.FirstName, LastName = userProfileStateManager.LastName, PhoneNo = userProfileStateManager.PhoneNo });

            FormsAuthentication.SetAuthCookie(userProfileStateManager.Email, false);

            var nextPage = ((PublishedContentModel)CurrentPage.GetProperty(Constants.FormContent.FieldNames.NextStep).Value).Id;
            userProfileStateManager.AddAllowedStep(nextPage);

            return RedirectToUmbracoPage(nextPage);
        }

        public ActionResult SendEmailAgain(string url, string parentid)
        {
            if (!userProfileStateManager.HasAcceptedTerms)
            {
                LogHelper.Error(this.GetType(), "User has not accepted the terms.", new Exception());
                return Redirect(url);
            }

            try
            {
                userProfileStateManager.EmailCode = accessTokenManager.Generate4DigitsRandomNo().ToString();
                emailManager.SendConfirmationEmail(userProfileStateManager.Email, userProfileStateManager.EmailCode, userProfileStateManager.FirstName, parentid);

                return Redirect(url);
            }
            catch (Exception ex)
            {
                LogHelper.Error(this.GetType(), ex.Message, ex);
                return Redirect(url);
            }
        }

        [HttpPost]
        public ActionResult CustomRightAreaStep(FormCollection collection, String pageId)
        {
            try
            {
                var formModel = new CustomRightAreaFormModel();
                TryUpdateModel(formModel);

                if (ModelState.IsValid)
                {
                    var token = Request.QueryString["accessToken"];
                    formModel.RightAreas = Request.Form["RightArea"];// here you'll get a string containing a list of checked values of the checkbox list separated by commas
                    pvsMemberRepository.UpdateMemberProperty(token, Constants.PvsMember.PvsMemberRightArea, formModel.RightAreas);

                    var nextPage = ((PublishedContentModel)CurrentPage.GetProperty(Constants.FormContent.FieldNames.NextStep).Value).Id;

                    var qs = new NameValueCollection();
                    qs.Add("accessToken", token);


                    //get Areas
                    var areasDataType = ApplicationContext.Services.DataTypeService.GetDataTypeDefinitionByName("PVS Member - RightArea - Checkbox list");
                    var generalTag = ApplicationContext.Services.DataTypeService.GetPreValuesCollectionByDataTypeId(areasDataType.Id).PreValuesAsDictionary.FirstOrDefault();

                   



                    // Create the userwidget Items
                    var memberService = Services.MemberService;
                    var contentService = Services.ContentService;

                    var currentMember = accessTokenManager.GetMemeberByToken(token);
                    var parentid = collection["parentid"];
                    var root = contentService.GetById(Int32.Parse(parentid));
                    var widgetPage = root.Descendants().Where(x => x.ContentType.Alias.Equals("widgetsPage")).FirstOrDefault();



                    var rawWidgets = contentService.GetById(widgetPage.Id).Children();
                    var userWidgetsItems = new List<Dictionary<string, object>>();
                    foreach (var widget in rawWidgets)
                    {
                        var jWidgetTags = new JArray();
                        var areaTags = widget.GetValue("pvsMemberRightArea");
                        // Collect the widget tags
                        foreach (var tags in areaTags.ToString().Split(','))
                        {
                            var tag = umbraco.library.GetPreValueAsString(int.Parse(tags.ToString()));
                            jWidgetTags.Add(tag);
                        }
                        // Member Tags
                        var jMemberTags = new JArray();
                        var memberAreaTags = currentMember.GetValue("pvsMemberRightArea");

                        // Collect the member tags
                        foreach (var tags in memberAreaTags.ToString().Split(','))
                        {
                            var tag = umbraco.library.GetPreValueAsString(int.Parse(tags.ToString()));
                            jMemberTags.Add(tag);
                        }

                        //var generalTag = umbraco.library.GetPreValueAsString(int.Parse(collection["generaltag"]));
                        //jMemberTags.Add(generalTag);
                        // check if the tags between widget and member matches
                        bool tagMatch = jMemberTags.Intersect(jWidgetTags).Count() > 0;

                        userWidgetsItems.Add(new Dictionary<string, object>() {
                            {
                            "tab",
                                1
                            },
                            {
                            "widgetId",
                                widget.Id
                            },
                            {
                            "widgetName",
                                widget.Name
                            },
                            {
                            "selected",
                                tagMatch ? 1 : 0
                            },
                            {
                            "open",
                                1
                            }
                        });
                    }
                    currentMember.SetValue("userWidget", JsonConvert.SerializeObject(userWidgetsItems));
                    memberService.Save(currentMember);
                    return RedirectToUmbracoPage(nextPage, qs);
                }
                else
                {
                    return RedirectToCurrentUmbracoPage();
                }

            }
            catch
            {
                return RedirectToCurrentUmbracoPage();
            }
        }

        [HttpPost]
        public ActionResult CustomCategoryStep(FormCollection collection)
        {
            try
            {
                var formModel = new CustomCategoryFormModel();
                TryUpdateModel(formModel);

                if (ModelState.IsValid)
                {
                    var token = Request.QueryString["accessToken"];
                    formModel.Category = Request.Form["Category"];
                    pvsMemberRepository.UpdateMemberProperty(token, Constants.PvsMember.PvsMemberCategory, formModel.Category);



                    var nextPage = ((PublishedContentModel)CurrentPage.GetProperty(Constants.FormContent.FieldNames.NextStep).Value).Id;
                    var qs = new NameValueCollection();
                    qs.Add("accessToken", token);
                    return RedirectToUmbracoPage(nextPage, qs);
                }
                else
                {
                    return RedirectToCurrentUmbracoPage();
                }

            }
            catch
            {
                return RedirectToCurrentUmbracoPage();
            }

        }
        [HttpPost]
        public ActionResult SkipCustomizeProfileStep(FormCollection collection)
        {

            var formCategoriModel = new CustomCategoryFormModel();
            var formAreaModel = new CustomRightAreaFormModel();
            TryUpdateModel(formAreaModel);
            TryUpdateModel(formCategoriModel);

            if (ModelState.IsValid)
            {
                var token = Request.QueryString["accessToken"];
                var umbracoHelper = new Umbraco.Web.UmbracoHelper();
                //get Areas
                var areasDataType = ApplicationContext.Services.DataTypeService.GetDataTypeDefinitionByName("PVS Member - RightArea - Checkbox list");
                var areasPreValues = ApplicationContext.Services.DataTypeService.GetPreValuesCollectionByDataTypeId(areasDataType.Id).PreValuesAsDictionary;

                List<string> areasvalueList = new List<string>();
                foreach (var value in areasPreValues)
                {
                    areasvalueList.Add(value.Value.Id.ToString());
                }
                var arrList = string.Join(",", areasvalueList.ToArray()); 
                // Get categories
                var dataType = ApplicationContext.Services.DataTypeService.GetDataTypeDefinitionByName("PVS Member - Category - Radio button list");
                var preValues = ApplicationContext.Services.DataTypeService.GetPreValuesCollectionByDataTypeId(dataType.Id).PreValuesAsDictionary.Last();
                formCategoriModel.Category = preValues.Value.Id.ToString();
                formAreaModel.RightAreas = arrList;

                pvsMemberRepository.UpdateMemberProperty(token, Constants.PvsMember.PvsMemberCategory, formCategoriModel.Category);
                pvsMemberRepository.UpdateMemberProperty(token, Constants.PvsMember.PvsMemberRightArea, formAreaModel.RightAreas);


                // Create the userwidget Items
                var memberService = Services.MemberService;
                var contentService = Services.ContentService;

                var currentMember = accessTokenManager.GetMemeberByToken(token);

                var parentid = collection["parentid"];
                var root = contentService.GetById(Int32.Parse(parentid));
                var widgetPage = root.Descendants().Where(x => x.ContentType.Alias.Equals("widgetsPage")).FirstOrDefault();

                var rawWidgets = contentService.GetById(widgetPage.Id).Children();
                var userWidgetsItems = new List<Dictionary<string, object>>();
                foreach (var widget in rawWidgets)
                {
                    var jWidgetTags = new JArray();
                    var areaTags = widget.GetValue("pvsMemberRightArea");
                    // Collect the widget tags
                    foreach (var tags in areaTags.ToString().Split(','))
                    {
                        var tag = umbraco.library.GetPreValueAsString(int.Parse(tags.ToString()));
                        jWidgetTags.Add(tag);
                    }
                    // Member Tags
                    var jMemberTags = new JArray();
                    var memberAreaTags = currentMember.GetValue("pvsMemberRightArea");

                    // Collect the member tags
                    foreach (var tags in memberAreaTags.ToString().Split(','))
                    {
                        var tag = umbraco.library.GetPreValueAsString(int.Parse(tags.ToString()));
                        jMemberTags.Add(tag);
                    }

                    // check if the tags between widget and member matches
                    bool tagMatch = jMemberTags.Intersect(jWidgetTags).Count() > 0;

                    userWidgetsItems.Add(new Dictionary<string, object>() {
                            {
                            "tab",
                                1
                            },
                            {
                            "widgetId",
                                widget.Id
                            },
                            {
                            "widgetName",
                                widget.Name
                            },
                            {
                            "selected",
                                tagMatch ? 1 : 0
                            },
                            {
                            "open",
                                1
                            }
                        });
                }
                currentMember.SetValue("userWidget", JsonConvert.SerializeObject(userWidgetsItems));
                memberService.Save(currentMember);



                var nextPage = ((PublishedContentModel)CurrentPage.GetProperty(Constants.PageContent.FieldNames.SkipProfileLinkField).Value).Id;

                var qs = new NameValueCollection();
                qs.Add("accessToken", token);
                return RedirectToUmbracoPage(nextPage, qs);
            }
            else
            {
                return RedirectToCurrentUmbracoPage();
            }



        }
    }
}