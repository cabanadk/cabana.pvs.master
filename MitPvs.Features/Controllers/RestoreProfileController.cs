﻿using System;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Web.Security;
using MitPvs.Feature.Managers;
using MitPvs.Feature.Models;
using MitPvs.Feature.Repositories;
using Umbraco.Core.Logging;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web.Mvc;

namespace MitPvs.Feature.Controllers
{
    public class RestoreProfileController : SurfaceController
    {
        private readonly IPvsMemberRepository pvsMemberRepository = new PvsMemberRepository();
        private readonly IAccessTokenManager accessTokenManager = new AccessTokenManager();
        private readonly IEmailManager emailService = new EmailManager();
        private readonly IUserProfileStateManager userProfileStateManager = new UserProfileStateManager();

        [HttpPost]
        public ActionResult SendCodeByEmail(RestoreProfileFormModel model)
        {
            if (string.IsNullOrEmpty(model.Email))
            {
                return RedirectToCurrentUmbracoPage();
            }

            if (!pvsMemberRepository.PvsMemberExists(model.Email))
            {
                var qs = new NameValueCollection();
                qs.Add("error", "1");
                return RedirectToCurrentUmbracoPage(qs);
            }

            userProfileStateManager.Email = model.Email;
            userProfileStateManager.EmailCode = accessTokenManager.Generate4DigitsRandomNo().ToString();

            emailService.SendRestoreEmail(userProfileStateManager.Email, userProfileStateManager.EmailCode, userProfileStateManager.FirstName, model.Parentid);

            return RedirectToCurrentUmbracoPage();
        }

        [HttpPost]
        public ActionResult ConfirmCode(RestoreProfileFormModel model)
        {
            if (model.EmailCode != userProfileStateManager.EmailCode.ToString() || model.Email.ToLower() != userProfileStateManager.Email.ToLower())
            {
                LogHelper.Error(this.GetType(), "User types incorrect email code or email", new Exception());
                var errorQs = new NameValueCollection();
                errorQs.Add("error", "2");
                return RedirectToCurrentUmbracoPage(errorQs);
            }

            userProfileStateManager.HasVerified = true;

            userProfileStateManager.AccessToken = pvsMemberRepository.GetPvsMember(userProfileStateManager.Email)?.GetValue(Constants.PvsMember.AccessTokenProperty).ToString();
            FormsAuthentication.SetAuthCookie(userProfileStateManager.Email, false);

            var nextPage = ((PublishedContentModel)CurrentPage.GetProperty(Constants.FormContent.FieldNames.NextStep).Value).Id;
            var qs = new NameValueCollection();
            qs.Add("accessToken", userProfileStateManager.AccessToken);
            return RedirectToUmbracoPage(nextPage, qs);

        }
    }
}