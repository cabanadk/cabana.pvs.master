﻿using MitPvs.Feature.Models;
using MitPvs.Feature.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace MitPvs.Feature.Controllers
{
    public class PvsDataController : UmbracoApiController
    {
        private readonly IPvsMemberRepository pvsMemberRepository = new PvsMemberRepository();

        public IHttpActionResult GetPvsMemberByEmail(string email)
        {
            var result = new PvsMemberModel();
            var member = pvsMemberRepository.GetPvsMember(email);
            if (member != null)
            {
                result.Email = member.Email;
                result.FirstName = member.GetValue(Constants.PvsMember.FirstNameProperty).ToString();
                result.LastName= member.GetValue(Constants.PvsMember.LastNameProperty).ToString();
                //result.PhoneNo= member.GetValue(Constants.PvsMember.TelephoneNumberProperty).ToString();
            }

            return Ok(result);
        }
    }
}