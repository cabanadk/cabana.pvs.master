﻿namespace MitPvs.Feature.Models
{
    public class PvsMemberModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        //public string PhoneNo { get; set; }
    }
}