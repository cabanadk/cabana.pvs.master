﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitPvs.Feature.Models
{
    public class RestoreProfileFormModel
    {
        public string Email { get; set; }
        public string EmailCode { get; set; }
        public string Parentid { get; set; }
    }
}