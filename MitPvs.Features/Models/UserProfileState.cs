﻿using System.Collections.Generic;

namespace MitPvs.Feature.Models
{
    public class UserProfileState : PvsMemberModel
    {
        public UserProfileState()
        {
            AllowedSteps = new List<int>();
        }

        public bool HasAcceptedTerms { get; set; }
        public string EmailCode { get; set; }
        public bool HasVerified { get; set; }
        public List<int> AllowedSteps { get; set; }
        public string AccessToken { get; set; }
    }
}