﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MitPvs.Feature.Models
{
    public class CustomCategoryFormModel
    {
        public string Email { get; set; }
        public string Category { get; set; }
    }
}