﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MitPvs.Feature.Models
{
    public class CustomRightAreaFormModel
    {
        public string Email { get; set; }
        public string RightAreas { get; set; }
        public string PageId { get; set; }
    }
}