﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MitPvs.Feature.Models
{
    public class AcceptTermsFormModel
    {
        public string Email { get; set; }
        [Required]
        public bool Accepted { get; set; }
        public string Parentid { get; set; }
    }
}