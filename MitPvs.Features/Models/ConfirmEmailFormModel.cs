﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MitPvs.Feature.Models
{
    public class ConfirmEmailFormModel
    {
        public string Email { get; set; }
        [Required]
        public string EmailCode { get; set; }
    }
}