﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MitPvs.Feature
{
    public struct Constants
    {
        public struct Media
        {
            public struct FieldNames
            {
                public static readonly string BackgroundImageField = "backgroundImage";
            }
        }

        public struct Footer
        {
            public struct FieldNames
            {
                public static readonly string FooterLinkLabel = "footerLinkLabel";
                public static readonly string FooterInformationTitle = "informationTitle";
                public static readonly string FooterInformationContent = "informationContent";
            }
        }

        public struct Header
        {
            public struct FieldNames
            {
                public static readonly string MetadataDescription = "metadataDescription";
                public static readonly string MetadataKeywords = "metadataKeywords";
                public static readonly string OpenGraphTitle = "openGraphTitle";
                public static readonly string OpenGraphType = "openGraphType";
                public static readonly string OpenGraphSiteName = "openGraphSiteName";
                public static readonly string OpenGraphImage = "openGraphImage";
            }
        }

        public struct PageContent
        {
            public struct FieldNames
            {
                public static readonly string BodyField = "body";
                public static readonly string CreateProfileButtonLabelField = "createProfileButtonLabel";
                public static readonly string RestoreProfileButtonLabelField = "restoreProfileButtonLabel";
                public static readonly string CreateProfileButtonLinkField = "createProfileButtonLink";
                public static readonly string RestoreProfileButtonLinkField = "restoreProfileButtonLink";
                public static readonly string CustomizeProfileButtonLabelField = "customizeProfileButtonLabel";
                public static readonly string CustomizeProfileButtonLinkField = "customizeProfileButtonLink";
                public static readonly string CustomizeSkipProfileButtonLabelField = "customizeProfileSkipButtonLabel";
                public static readonly string CustomizeSkipProfileButtonLinkField = "customizeProfileSkipButtonLink";
                public static readonly string SkipProfileLinkField = "customizeProfileSkipButtonLink";
            }
        }

        public struct FormContent
        {
            public struct FieldNames
            {
                public static readonly string HintText = "formHintText";
                public static readonly string ButtonText = "formButtonText";
                public static readonly string NextStep = "nextStep";
            }
        }

        public struct Terms
        {
            public struct FieldNames
            {
                public static readonly string AcceptTermsText = "acceptTermsText";
                public static readonly string RejectTermsButtonText = "rejectTermsButtonText";
            }
        }

        public struct PvsMember
        {
            public static readonly string MemberTypeName = "pVSMember";
            public static readonly string FirstNameProperty = "pvsMemberFirstName";
            public static readonly string LastNameProperty = "pvsMemberLastName";
            //public static readonly string TelephoneNumberProperty = "pvsMemberTelephoneNumber";
            public static readonly string AlternativeEmailProperty = "pvsAlternativeEmail";
            public static readonly string AccessTokenProperty = "pvsAccessToken";
            public static readonly string AccessTokenExpirationTimeProperty = "pvsAccessTokenCreatedTime";
            public static readonly string PvsMemberGroupName = "PVS Member Group";
            public static readonly string PvsMemberRightArea = "pvsMemberRightArea";
            public static readonly string PvsMemberCategory= "pvsMemberCategory";
        }

        public struct SiteSettings
        {
            public static readonly string FirstStepPage = "firstStepPage";
            public static readonly string EmailFromAddress = "emailFromAddress";
            public static readonly string VerifyCodeEmailSubject = "verifyCodeEmailSubject";
            public static readonly string VerifyCodeEmailBodyText = "verifyCodeEmailBodyText";
            public static readonly string RestoreCodeEmailSubject = "restoreCodeEmailSubject";
            public static readonly string RestoreCodeEmailBodyText = "restoreCodeEmailBodyText";
        }

        public struct FormId
        {
            public static readonly Guid AlternativeEmailFormId = new Guid("090d0680-3bd3-47f2-b313-f57dcc8198cd");
        }

        public struct ViewPath
        {
            public static readonly string Header = "~/views/partials/Header.cshtml";
            public static readonly string CanAccess = "~/views/partials/CanAccessStep.cshtml";
            public static readonly string Protected = "~/views/partials/Protected.cshtml";
            public static readonly string ProfileDetailsStepForm = "~/views/CreateProfile/ProfileDetailsStepForm.cshtml";
            public static readonly string AcceptTermsStepForm = "~/views/CreateProfile/AcceptTermsStepForm.cshtml";
            public static readonly string ConfirmEmailStepForm = "~/views/CreateProfile/ConfirmEmailStepForm.cshtml";
            public static readonly string CustomRightAreaStepForm = "~/views/CreateProfile/CustomRightAreaStepForm.cshtml";
            public static readonly string CustomCategoryStepForm = "~/views/CreateProfile/CustomCategoryStepForm.cshtml";
            public static readonly string RestoreProfileByEmailForm = "~/views/CreateProfile/RestoreProfileByEmailForm.cshtml";
        }
    }
}