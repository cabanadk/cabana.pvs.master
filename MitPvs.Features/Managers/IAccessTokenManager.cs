﻿using Umbraco.Core.Models;

namespace MitPvs.Feature.Managers
{
    public interface IAccessTokenManager
    {
        string GenerateToken();
        IMember GetMemeberByToken(string token);
        long GetTimeSpan();
        bool IsTokenExpired(long createdTimeTicks);
        bool IsTokenExpired(string token);
        int Generate4DigitsRandomNo();
    }
}