﻿using System;
using System.Linq;
using System.Net.Mail;
using System.Text;
using umbraco;
using umbraco.NodeFactory;
using Umbraco.Web;

namespace MitPvs.Feature.Managers
{
    public class StringExtension
    {   // replaces the placeholders from Umbraco RTE to the dynamic text from input by user.
        public string FormatMail(string body, string code, string FirstName)
        {
            StringBuilder formattetMail = new StringBuilder(body);
            formattetMail.Replace("[firstName]", FirstName);
            formattetMail.Replace("[code]", code);
            
            return formattetMail.ToString();
        }
    }
    public class EmailManager : IEmailManager
    {
        public void SendConfirmationEmail(string emailAddress, string code, string FirstName, string partenid)
        {
            var root = new Node(Int32.Parse(partenid));
            var emailFrom = root.GetProperty(Constants.SiteSettings.EmailFromAddress).ToString();
            var emailSubject = root.GetProperty(Constants.SiteSettings.VerifyCodeEmailSubject).ToString();
            var emailBody = root.GetProperty(Constants.SiteSettings.VerifyCodeEmailBodyText).ToString();

            StringExtension formattetBody = new StringExtension();
            string emailSubjectFormattet = formattetBody.FormatMail(emailSubject, code, FirstName);
            string emailBodyFormattet = formattetBody.FormatMail(emailBody, code, FirstName);

            MailMessage email = new MailMessage(emailFrom, emailAddress);
            email.IsBodyHtml = true;
            email.Subject = emailSubjectFormattet;
            email.Body = emailBodyFormattet;

            try
            {
                //Connect to SMTP credentials set in web.config
                SmtpClient smtp = new SmtpClient();
                //Try & send the email with the SMTP settings
                smtp.Send(email);
            }
            catch (Exception ex)
            {
                //Throw an exception if there is a problem sending the email
                throw ex;
            }

        }

        public void SendRestoreEmail(string emailAddress, string code, string FirstName, string partenid)
        {
            var root = new Node(Int32.Parse(partenid));
            var emailFrom = root.GetProperty(Constants.SiteSettings.EmailFromAddress).ToString();
            var emailSubject = root.GetProperty(Constants.SiteSettings.VerifyCodeEmailSubject).ToString();
            var emailBody = root.GetProperty(Constants.SiteSettings.VerifyCodeEmailBodyText).ToString();

            StringExtension formattetBody = new StringExtension();
            string emailSubjectFormattet = formattetBody.FormatMail(emailSubject, code, FirstName);
            string emailBodyFormattet = formattetBody.FormatMail(emailBody, code, FirstName);

            MailMessage email = new MailMessage(emailFrom, emailAddress);
            email.IsBodyHtml = true;
            email.Subject = emailSubjectFormattet;
            email.Body = emailBodyFormattet;

            try
            {
                //Connect to SMTP credentials set in web.config
                SmtpClient smtp = new SmtpClient();
                //Try & send the email with the SMTP settings
                smtp.Send(email);
            }
            catch (Exception ex)
            {
                //Throw an exception if there is a problem sending the email
                throw ex;
            }

        }
    }
}