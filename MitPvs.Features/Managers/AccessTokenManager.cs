﻿using System;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Core.Services;

namespace MitPvs.Feature.Managers
{
    public class AccessTokenManager : IAccessTokenManager
    {
        private IMemberService memberService = ApplicationContext.Current.Services.MemberService;

        public IMember GetMemeberByToken(string token)
        {
            return memberService.GetMembersByPropertyValue(Constants.PvsMember.AccessTokenProperty, token).FirstOrDefault();
        }


        public string GenerateToken()
        {
            return CalculateHash(GenerateString(10)).Substring(0, 32);
        }

        public bool IsTokenExpired(string token)
        {
            var member = GetMemeberByToken(token);
            if (member == null)
            {
                return true;
            }

            return IsTokenExpired(Convert.ToInt64(member.GetValue(Constants.PvsMember.AccessTokenExpirationTimeProperty)));
        }

        public bool IsTokenExpired(long createdTimeTicks)
        {
            var createdTime = new DateTime(createdTimeTicks);
            TimeSpan elapsedSpan = DateTime.Now.Subtract(createdTime);
            var validSeconds = Convert.ToInt64(ConfigurationManager.AppSettings["memberAccessTokenValidSeconds"]);
            return elapsedSpan.TotalSeconds > validSeconds; 
        }

        public long GetTimeSpan()
        {
            DateTime dt1970 = new DateTime(1970, 1, 1);
            DateTime current = DateTime.Now;
            TimeSpan span = current - dt1970;
            return span.Ticks;
        }

        public int Generate4DigitsRandomNo()
        {
            int _min = 1000;
            int _max = 9999;
            Random _rdm = new Random();
            return _rdm.Next(_min, _max);
        }

        private string GenerateString(int length)
        {
            var AllowableCharacters = "abcdefghijklmnopqrstuvwxyz0123456789";

            var bytes = new byte[length];

            using (var random = RandomNumberGenerator.Create())
            {
                random.GetBytes(bytes);
            }

            return new string(bytes.Select(x => AllowableCharacters[x % AllowableCharacters.Length]).ToArray());
        }

        private string CalculateHash(string input)
        {
            using (var algorithm = SHA512.Create()) //or MD5 SHA256 etc.
            {
                var hashedBytes = algorithm.ComputeHash(Encoding.UTF8.GetBytes(input));

                return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            }
        }
    }
}