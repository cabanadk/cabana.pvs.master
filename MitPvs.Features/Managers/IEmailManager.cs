﻿using MitPvs.Feature.Models;

namespace MitPvs.Feature.Managers
{
    public interface IEmailManager
    {
        void SendConfirmationEmail(string emailAddress, string code, string firstNamem, string partenid);
        void SendRestoreEmail(string emailAddress, string code, string firstName, string partenid);
    }
}