﻿namespace MitPvs.Feature.Managers
{
    public interface IFlowManager
    {
        bool IsStepValid();

        string GetFirstStepUrl();
    }
}