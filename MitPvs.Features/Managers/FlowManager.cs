﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web;

namespace MitPvs.Feature.Managers
{
    public class FlowManager : IFlowManager
    {
        private readonly IUserProfileStateManager userProfileStateManager = new UserProfileStateManager();
        public bool IsStepValid()
        {
            var currentPageId = UmbracoContext.Current.PageId;
            return currentPageId.HasValue && userProfileStateManager.AllowedSteps.Contains(currentPageId.Value);
        }

        public string GetFirstStepUrl()
        {
            var umbracoContext = UmbracoContext.Current;
            var umbracoHelper = new UmbracoHelper(umbracoContext);

            var homePage = umbracoHelper.TypedContentAtRoot().FirstOrDefault(c => c.HasProperty(Constants.SiteSettings.FirstStepPage)).GetPropertyValue<int>(Constants.SiteSettings.FirstStepPage);
            return umbracoHelper.TypedContent(homePage).Url;
        }
    }
}