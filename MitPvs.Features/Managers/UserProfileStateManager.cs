﻿using MitPvs.Feature.Models;
using System.Collections.Generic;
using System.Web;

namespace MitPvs.Feature.Managers
{
    public class UserProfileStateManager : IUserProfileStateManager
    {
        private UserProfileState state;
        private static readonly string sessionKey = "UserProfileStateManagerKey";

        private static UserProfileState GetState()
        {
            if(HttpContext.Current.Session[sessionKey] as UserProfileState == null)
            {
                HttpContext.Current.Session[sessionKey] = new UserProfileState();
            }

            return HttpContext.Current.Session[sessionKey] as UserProfileState;
        }

        public List<int> AllowedSteps
        {
            get
            {
                return GetState().AllowedSteps;
            }
        }

        public void AddAllowedStep(int step)
        {
            AllowedSteps.Add(step);
        }

        public string FirstName
        {
            get
            {
                return GetState().FirstName;
            }
            set
            {
                GetState().FirstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return GetState().LastName;
            }
            set
            {
                GetState().LastName = value;
            }
        }

        //public string PhoneNo
        //{
        //    get
        //    {
        //        return GetState().PhoneNo;
        //    }
        //    set
        //    {
        //        GetState().PhoneNo = value;
        //    }
        //}

        public string Email
        {
            get
            {
                return GetState().Email;
            }
            set
            {
                GetState().Email = value;
            }
        }
        public bool HasAcceptedTerms
        {
            get
            {
                return GetState().HasAcceptedTerms;
            }
            set
            {
                GetState().HasAcceptedTerms = value;
            }
        }

        public string EmailCode
        {
            get
            {
                return GetState().EmailCode;
            }
            set
            {
                GetState().EmailCode = value;
            }
        }

        public bool HasVerified
        {
            get
            {
                return GetState().HasVerified;
            }
            set
            {
                GetState().HasVerified = value;
            }
        }

        public string AccessToken
        {
            get
            {
                return GetState().AccessToken;
            }
            set
            {
                GetState().AccessToken = value;
            }
        }
    }
}