﻿using System.Collections.Generic;

namespace MitPvs.Feature.Managers
{
    public interface IUserProfileStateManager
    {
        List<int> AllowedSteps { get; }
        void AddAllowedStep(int step);
        string EmailCode { get; set; }
        string Email { get; set; }
        string FirstName { get; set; }
        bool HasAcceptedTerms { get; set; }
        bool HasVerified { get; set; }
        string LastName { get; set; }
        //string PhoneNo { get; set; }
        string AccessToken { get; set; }
    }
}