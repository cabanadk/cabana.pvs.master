$(function () {
    'use strict';

    var fields = $('.code');
    
    if (fields.length > 0) {
        window.setInterval(function () {
            var reqlength = $('.code').length;
            var value = $('.code').filter(function () {
                return this.value !== '';
            });

            if (value.length >= 0 && (value.length !== reqlength)) {
                $('.confirm-code').attr("disabled", true);
            } else {
                $('.confirm-code').attr("disabled", false);
            }
        }, 700);
    }
    

    $(document).on("click", ".code", function () {
        $(this).val("");
    });
});