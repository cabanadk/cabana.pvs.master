(function($) {

    if (!$('.accordion').length) {
        return;
    }

   $('.toggle').click(function(e) {
      e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
          $this.children('.checkmark').toggleClass('checked');
          
          $this.next().css('opacity', 0);
          
      } else {
          $('.checkmark').removeClass('checked');
          $this.children('.checkmark').toggleClass('checked');
          $this.parent().parent().find('li .inner').css('opacity', 0);
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(250);
          $this.next().toggleClass('show');
          $this.next().delay( 300 ).slideToggle(500);
          $this.next().animate({
          opacity: 1,
          }, 100, function() {
            // Animation complete.
          });
          }
  });


})(jQuery);