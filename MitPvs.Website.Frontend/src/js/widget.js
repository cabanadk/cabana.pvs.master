(function($) {
    if (!$('.widget').length) {
        return;
    }
    $(document).on("click", ".options .icon-widget-more", function(e) {
        e.preventDefault();
        if (!$(this).closest('.widget').find('.options-wrapper').is(':visible')) {
            $(this).closest('.widget').find('.options-wrapper').slideToggle(0).css('display', 'flex');
        } else {
            $(this).closest('.widget').find('.options-wrapper').slideUp(150);
        }
    });

    $(document).on("click", ".collapse-widget", function(e) {
        if (!$(this).closest('.widget').find('.widget-content').is(':visible')) {
            $(this).closest('.widget').find('.widget-content').slideToggle(350);
            $(this).closest('.widget').find('.widget-menu').slideToggle(350);
            $(this).closest('.widget').find('.widget-footer').slideToggle(350);
            $(this).closest('.widget').toggleClass('hidden-widget');
            $(this).find('i').removeClass('rotate');
        } else {
            $(this).closest('.widget').find('.widget-content').slideUp(350);
            $(this).closest('.widget').find('.widget-menu').slideUp(350);
            $(this).closest('.widget').find('.widget-footer').slideUp(350);
            $(this).closest('.widget').toggleClass('hidden-widget');
            $(this).find('i').addClass('rotate');
        }
        e.preventDefault();
    });


    $(document).on("click", ".icon-widget-arrow-up", function(e) {
        e.preventDefault();
        if ($(this).attr('data-original-title') == 'Luk') {
            $(this).attr('data-original-title', 'Åben');
        } else {
            $(this).attr('data-original-title', 'Luk');
        }
    });

})(jQuery);