(function ($) {
	
  	if (!$('form').length) {
  	    return;
  	}
  	
  	$( ".form-control" ).keyup(function() {
	  if (!$(this).is(":valid") && $(this).val() ) {
	  	$(this).parent().addClass('notValid');
	  } else {
	  	$(this).parent().removeClass('notValid');
	  }
	});

  	$(".input-link").keyup(function(){
  		var urlInput = $(this).val();
  		var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
		var regex = new RegExp(expression);

	    console.log(urlInput)

	    if (urlInput.match(regex)) {
	    	$(this).parent().removeClass('notValid');
	    } else {
	    	$(this).parent().addClass('notValid');
	    }
	});



 })(jQuery);