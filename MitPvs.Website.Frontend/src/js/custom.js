﻿
$(function () {
    var token = $("div.loading-dots").data("token");

    if (token) {
        setTimeout(
        function () {
            window.location = "/CustomizeProfile?accessToken=" + token;
        }, 5000);
    }
});


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
console.log(Cookies.get('sendcode'))
var emailsuccess = getUrlParameter("error");
var code = getUrlParameter("error");
//// success on button for send code
if (emailsuccess === "1" && Cookies.get('sendcode')) {
    Cookies.remove('sendcode');
    $("#resendCode").hide();
} else if (Cookies.get('sendcode') && $("#sendCodeButton").length > 0) {
    $("#sendCodeButton").addClass("success");
    $("#resendCode").show();
} else if (code !== "2") {
    Cookies.remove('sendcode');
    Cookies.remove('codeverifyed');
} 

$(document).on("click", "#sendCodeButton", function () {
    Cookies.set('sendcode', true);
});


$(document).on("click", "#confirm-code", function () {
    Cookies.set('codeverifyed', true);
});


$(function () {
    var token = $("div.signup-done").data("token");
    if (token) {
        setTimeout(
        function () {
            window.location = "/widgets/#/" + token;
        }, 5000);
    }
});

$(function () {
    var prevButton = $("i#prev-button");
    if (prevButton) {
        prevButton.click(function (){
            window.history.back();
        });
    }
});

$(function () {
    var validForm = false;
    $("#profile-details-form").submit(function (e) {
        if (!validForm) {
            e.preventDefault();
            var form = $(this);
            var emailField = form.find('input[name="Email"]');
            var confirmEmailField = form.find('input[name="ConfirmEmail"]');

            $(emailField).removeClass("is-invalid");
            $(confirmEmailField).removeClass("is-invalid");

            var email = emailField.val();
            var confirmEmail = confirmEmailField.val();
            if (email !== confirmEmail) {
                $(confirmEmailField).addClass("is-invalid");
            } else {
                $.get("/umbraco/Api/pvsdata/getpvsmemberbyemail?email=" + email, function (data) {
                    if (data.Email) {
                        $(form.find('input[name="Email"]')).addClass("is-invalid");
                    } else {
                        validForm = true;
                        form.submit();
                    }
                });
            }
        }
    });
});

function profileDetailsForm() {
    var profileDetailsForm = $("#profile-details-form");
    if (profileDetailsForm.find("input[type='email']").val() !== "" && profileDetailsForm.find('input[name="FirstName"]').val() !== "" && profileDetailsForm.find('input[name="LastName"]').val() !== "" && profileDetailsForm.find('input[name="ConfirmEmail"]').val() !== "") {
        profileDetailsForm.find('input[type="submit"]').prop('disabled', false);
    } else {
        profileDetailsForm.find('input[type="submit"]').prop('disabled', true);
    }
}


function acceptTerms() {
    var acceptTermsForm = $("#accept-terms-form");
    if (acceptTermsForm.find("input[type='checkbox']").prop("checked")) {
        acceptTermsForm.find('input[name="Accepted"]').val(true);
        acceptTermsForm.find('input[type="submit"]').prop('disabled', false);
    } else {
        acceptTermsForm.find('input[name="Accepted"]').val(false);
        acceptTermsForm.find('input[type="submit"]').prop('disabled', true);
    }
}

$(function () {
    $("#confirm-email-form").submit(function (e) {
        var form = $(this);
        var code = form.find('input[name="first"]').val() + form.find('input[name="second"]').val() + form.find('input[name="third"]').val() + form.find('input[name="fourth"]').val();
        form.find('input[name="EmailCode"]').val(code);
    });
});

function areaValidat() {
    var areaForm = $("#category-form");
    
    if ($('.c-card:checked').length > 0) {
        areaForm.find('input[type="submit"]').prop('disabled', false);
        
    } else {
        areaForm.find('input[type="submit"]').prop('disabled', true);
    }
}
$(document).on("click", ".c-card", function () {
    areaValidat();
});


function categoriValidat() {
    var categoriForm = $('form');

    if ($('.checked').length > 0) {
        categoriForm.find('input[name="Accepted"]').val(true);
        categoriForm.find('input[type="submit"]').prop('disabled', false);

    } else {
        categoriForm.find('input[name="Accepted"]').val(false);
        categoriForm.find('input[type="submit"]').prop('disabled', true);
    }
}
$(document).on("click", ".radio-list li", function () {
    categoriValidat();
});

// Set parent id to hidden field on accept terms page
$(function () {
    var parentId = $("#pageId").data("currentpageid");
    if (parentId) {
        $("#parentidfield").val(parentId);
    }
});